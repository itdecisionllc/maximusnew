$(function() {

    // anchor
    $('a[href*="#"]').click(function(e){
        e.preventDefault();
        var anchor = $(this),
            url = anchor.attr('href');

        if ( (url == '#') || (url == '#0') ){
            return true;
        }else{
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top - 40
            }, 1000);
        }
    });
})